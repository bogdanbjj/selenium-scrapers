import unicodecsv as csv
import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class PCNPMODoctorSpider(object):
    url = 'https://www.pcnpmo.ca/alberta-pcns/pages/map.aspx?redirect=edmoareadocs&zone' \
          '=Edmonton&tab=doctor&redirect=edmoareadocs&zone=Edmonton&tab=doctor'
    file_exists = False
    name = 'Canadian Clinics'

    def __init__(self):
        self.c = webdriver.Chrome()
        self.search()
        for i in range(68):
            self.extract_info()
            try:
                WebDriverWait(self.c, 30).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'round-right'))
                    )
                # import ipdb;ipdb.set_trace()
                n_page = self.c.find_element_by_xpath("//a[@class='round-right']")
                n_page.click()
            except NoSuchElementException:
                print 'CRASH!!!!\n' * 3
                break
            except Exception as e:
                print
                print 'An execption occured: ', str(e)
                print
                if self.c:
                    self.c.close()

    def search(self):
        self.c.get(self.url)
        time.sleep(20)
        checkbox = self.c.find_element_by_id('acceptingNewPatientsCheckbox')
        search = self.c.find_element_by_id("doctorSearchButton")
        # uncheck checkbox
        # import ipdb; ipdb.set_trace()
        checkbox.click()
        time.sleep(1.5)
        search.click()

    def extract_info(self):
        time.sleep(10)
        name = self.c.find_elements_by_xpath("//span[@class='clinic-title']")
        name = [n.text for n in name]
        address = self.c.find_elements_by_xpath("//div[@class='col twelve clinic-result']/div/span[not(@class)][1]")
        address = [a.text for a in address
                    if a.text[:3].isdigit() or '#' in a.text or 'ave' in a.text.lower() or 'street' in a.text.lower()
                   or 'st' in a.text.lower()
                   ]
        city = self.c.find_elements_by_xpath("//div[@class='col twelve clinic-result']/div/span[not(@class)][2]")
        city = [c.text for c in city]
        phone = self.c.find_elements_by_xpath("//div[@class='col twelve clinic-result']/div/span[not(@class)][3]")
        phone = [p.text for p in phone]
        lat_long = self.c.find_elements_by_xpath("//div[@class='clinic-static-map']/img")
        lat_long = self.process_lat_and_long(lat_long)

        data = {
            'Name': name, 'Address': address, 'City': city, 'Phone': phone,
            'Latitude': [i[0] for i in lat_long], 'Longitude': [i[1] for i in lat_long]
        }

        for i in range(len(name)):
            d = {}
            try:
                d['Name'] = name[i]
                d['Address'] = address[i]
                d['City'] = city[i]
                d['Phone'] = phone[i]
                d['Latitude'] = lat_long[i][0]
                d['Longitude'] = lat_long[i][1]
            except IndexError:
                pass
                # import ipdb; ipdb.set_trace()
            self.write_to_csv(data=d)

    @staticmethod
    def process_lat_and_long(data):
        lat_long = []
        for d in data:
            lat, long = d.get_attribute('src').split("=")[1].split("&")[0].split(',')
            lat_long.append((lat, long))

        return lat_long

    def write_to_csv(self, data):
        with open(self.name + '.csv', 'ab') as f:
            w = csv.DictWriter(f, sorted(data.keys()), delimiter=' ')
            if not self.file_exists:
                w.writeheader()
                self.file_exists = True
            w.writerow(data)

PCNPMODoctorSpider()