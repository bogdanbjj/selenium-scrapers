import random
import time

import requests
from bs4 import BeautifulSoup
from lxml import html as ht
from selenium import webdriver

NAME = 'wangdong8790@gmail.com'
PSWD = 'bps90(),.'
HOME_URL = 'https://www.linkedin.com'
neil = 'https://www.linkedin.com/in/neil-barofsky-85033745'


class LinkedinSpider(object):
    ALL_PPL = []
    VISITED = {}

    def __init__(self):
        """
         - starts a new requests session
         - gets the html content of the home page
         - gets the cookie
         - starts selenium driver
         - driver gets homepage
         - adds csrf cookie to browser
         - login (although not sure about that)
         - gets a random linkedin profile; in this case
         it's 'our good friend' neil who is connected
         to some influencial people who have a lot
         of followers, but that doesn't matter because
         we can't get their urls.
         Once we opened 'neil' we have our entry point
         to some urls; extract those and add them to a list
         and then crawl those and repeat procedure until
         the spider gets banned or we scrape everything
        """
        client = requests.session()
        content = client.get(url=HOME_URL).content
        csrf_cookie = self.requests_get_cookie(content)
        self.browser = webdriver.Chrome()
        self.browser.get(HOME_URL)
        self.add_cookie(csrf_cookie)
        # self.login()
        self.get_neil(url=neil)
        time.sleep(2)
        self.extract_links_not_logged_in()
        self.crawl()

    def webdriver_proxy(self, proxy):
        import ipdb;ipdb.set_trace()
        proxy, port = proxy.split(" ")
        self.driver = webdriver.FirefoxProfile()
        self.driver.set_preference("network.proxy.type", 1)
        self.driver.set_preference("network.proxy.http", random.choice(proxy))
        self.driver.set_preference("network.proxy.http_port", random.choice(port))
        self.driver.update_preferences()

    def login(self):
        name = self.browser.find_element_by_name("session_key")
        pswd = self.browser.find_element_by_name('session_password')
        name.send_keys(NAME)
        pswd.send_keys(PSWD)
        pswd.submit()

    def add_cookie(self, cookie):
        self.browser.add_cookie(
            {'name': 'loginCsrfParam-login', 'value': cookie})

    def requests_get_cookie(self, content):
        # get the cookie
        html = BeautifulSoup(content, 'lxml')
        csrf_cookie = html.find(id='loginCsrfParam-login')['value']
        return csrf_cookie

    def get_neil(self, url):
        """funny name"""
        self.browser.get(url)

    def extract_links_logged_in(self):
        response = ht.fromstring(self.browser.page_source)
        also_viewed_people = response.xpath("//a[@class='browse-map-photo']/@href")
        more_influencers = response.xpath("//a[@class='browse-map-photo']/@href")
        self.ALL_PPL.extend(also_viewed_people)
        self.ALL_PPL.extend(more_influencers)

    def extract_links_not_logged_in(self):
        """Because not logged in cannot see more influencers"""
        response = ht.fromstring(self.browser.page_source)
        also_viewed_people =response.xpath("//h4[@class='item-title']/a/@href")
        self.ALL_PPL.extend(also_viewed_people)

    def crawl(self):
        import ipdb; ipdb.set_trace()
        start_time = time.time()
        while self.ALL_PPL:
            person_url = self.ALL_PPL.pop()
            if self.VISITED.get(person_url, 0):
                continue

            self.VISITED[person_url] = True
            self.browser.get(person_url)
            time.sleep(random.uniform(2.7, 6.5))
            self.extract_links_not_logged_in()
            self.elapsed_time = time.time() - start_time
            print '"Scraped" {} items in {} seconds'.format(len(self.VISITED.keys()), self.elapsed_time)
            print self.browser.desired_capabilities

    def close(self):
        self.browser.close()


ls = LinkedinSpider()
ls.close()
print len(ls.VISITED.keys())
print len(ls.ALL_PPL)
print len(set(ls.ALL_PPL))
print 'number of duplicates in the list is: ', len(ls.ALL_PPL) - len(set(ls.ALL_PPL))
print 'scraped '

# html.parse(filename_Or_url)
