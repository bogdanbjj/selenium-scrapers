import time

from lxml import html

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, ElementNotVisibleException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import xlwt


class GiacometiArtSpider(object):
    base_url = 'http://www.fondation-giacometti.fr'
    start_url = 'http://www.fondation-giacometti.fr/en/art/16/discover-the-artwork/18/alberto-giacometti-database/19/all-works/#?ref=database&open=all'
    ajax_call = 'http://www.fondation-giacometti.fr/en/content/art/16/ajax_1/18/ajax_2/19/ajax_3/'
    artist_name = 'Alberto Giacometti'

    def __init__(self):
        """Cannot simulate ajax call to
            load data. Will need to do it the
            old fashioned way. Ups my math was wrong. I does not
            take 2 hours to open index page. It runs a lot faster than
            that. But it would take a long time to open
            every page and extarct info
        """
        self.all_info = []
        self.b = webdriver.Chrome()
        self.b.get(self.start_url)
        time.sleep(5)
        try:
            for i in range(74):
                print "Currently at page ", i + 1
                self.get_more()
        except ElementNotVisibleException:
            print "This is the end!"
            self.get_info()
            info = self.all_info
            print len(info)

            write_to_excel(info)
        finally:
            if self.b:
                self.b.quit()

    def get_more(self):
        """
        Gets the 'next page' and wait to make sure
        data is loaded
        :param: just itself
        :return: None
        """
        # import ipdb; ipdb.set_trace()
        # elem = WebDriverWait(self.b, 10).until(EC.element_to_be_clickable((By.ID, 'db-works-more')))
        elem = WebDriverWait(self.b, 10).until(EC.presence_of_element_located((By.ID, 'db-works-more')))
        time.sleep(5)
        elem.click()

    def get_info(self):
        """
        Method called after all the pages have been loaded.
        Extracts all information and packages nietly in a dict
        for further processing
        :return: None
        """
        response = html.fromstring(self.b.page_source)
        # selenium gets everything, including the ones where there no title
        titles = self.b.find_elements_by_xpath("//a[@class='work-title link']")
        # titles = response.xpath("//a[@class='work-title link']/text()")

        links = response.xpath(
            "//a[@class='db-result-img margin0 BG2 tableCell fullH fullW txtM flowhidden']/@href")
        links = [(self.base_url + link) for link in links]

        agd = response.xpath("//li[@class='bold']/text()")
        creation_date = response.xpath("//li[@class='TpaddingSm']/text()")
        tech_and_dim = response.xpath("//ul[@class='txtL LpaddingLg lhReg']/li[4]/text()")

        images = response.xpath(
            "//a[@class]/img[@class='HmarginAuto dispBlock MXheight140 MXwidth140 alignCenter']/@src")
        img = [("{}" + img).format(self.base_url) for img in images]

        # writes to excel part
        for i in range(len(images)):
            item = {}
            item['title'] = titles[i].text if titles[i].text else 'Unknown name'
            item['artist'] = self.artist_name
            item['agd'] = agd[i]
            try:
                item['creation_date'] = creation_date[i]
            except IndexError:
                print 'creation_date length is: ', len(creation_date)
                item['creation_date'] = ''

            try:
                technique, dimensions = tech_and_dim[i].split(',', 1)
                item['technique'] = technique if technique else ''
                item['dimensions'] = dimensions.strip() if dimensions else ''
            except ValueError:
                # in this case there is no dimension
                item['technique'] = tech_and_dim[i]
                item['dimensions'] = 'Unknown dimensions'

            item['image'] = img[i]
            item['url'] = links[i]
            self.all_info.append(item)


def write_to_excel(item):
    """
    Name says it all.
    :param item:
    :return:
    """
    it = item[0]
    col = 1
    book, sheet = create_sheet(it)

    for i in item:
        sheet.write(col, 0, i.get('artist'))
        sheet.write(col, 1, i.get('title'))
        sheet.write(col, 2, i.get('creation_date'))
        sheet.write(col, 3, i.get('dimensions'))
        sheet.write(col, 4, i.get('technique'))
        sheet.write(col, 5, i.get('agd'))
        sheet.write(col, 6, i.get('image'))
        sheet.write(col, 7, i.get('url'))
        col += 1

    book.save("Giacometti.xls")


def create_sheet(it):
    """
    Helper function that creates
    workbook and sheet, writes
    column names to sheet.
    """
    book = xlwt.Workbook()
    sheet = book.add_sheet('Foundation-Giacometti')
    sheet.write(0, 0, "Artist")
    sheet.write(0, 1, "Title")
    sheet.write(0, 2, "Creation date")
    sheet.write(0, 3, "Dimensions")
    sheet.write(0, 4, "Technique")
    sheet.write(0, 5, "Agd")
    sheet.write(0, 6, "Image")
    sheet.write(0, 7, "Url")
    return book, sheet


gc = GiacometiArtSpider()