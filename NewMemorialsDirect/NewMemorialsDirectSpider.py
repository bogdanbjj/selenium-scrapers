from lxml.html import fromstring
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select

import time
import unicodecsv as csv


from selenium.webdriver.support.wait import WebDriverWait


class NewMemorialsDirectItemSpider(object):
    start_urls = ['https://www.newmemorialsdirect.com/login.aspx']
    pages = [
        'http://www.newmemorialsdirect.com/wholesale-cremation-jewelry.aspx?page={}'.format(i)
        for i in range(2, 12)
    ]
    base_url = 'https://www.newmemorialsdirect.com'
    DROPDOWNS = {}
    file_exists = False

    def __init__(self):
        self.c = webdriver.Chrome()
        c = self.c
        self.login(c)
        self.open_pages()

    def login(self, c):
        """c is a webdriver instance"""
        c.get(self.start_urls[0])
        name = c.find_element_by_name('ctl00$pageContent$loginRegister$txtEmail')
        pswd = c.find_element_by_name('ctl00$pageContent$loginRegister$txtPassword')
        # time.sleep(1)
        name.send_keys('james@afterlifeessentials.com')
        # time.sleep(0.7)
        pswd.send_keys('nmdessentials')
        button = c.find_element_by_name('ctl00$pageContent$loginRegister$imbSignIn')
        button.click()
        # time.sleep(0.6)

    def open_pages(self):
        pages = self.pages
        while pages:
            p = pages.pop(0)
            self.parse_page(p)
            time.sleep(20)

        return

    def parse_page(self, page):
        self.c.get(page)
        html = fromstring(self.c.page_source)
        items = html.xpath("//div[@class='product-list-control']/a/@href")
        for item_url in items:
            url = self.base_url + item_url
            self.select_from_dropdown(url)

        return

    def select_from_dropdown(self, it):
        self.c.get(it)

        dropdowns = self.c.find_elements_by_xpath(
            "//div/table/tbody/tr/td/div[@class='variationDropdownPanel']/select")

        if not dropdowns:
            self.extract_data(self.c.page_source)

        if len(dropdowns) == 1:
            self.one_loop(dropdowns)

        if len(dropdowns) == 2:
            self.two_loops(dropdowns)

        return

    def one_loop(self, dropdown):
        sel = Select(dropdown[0])
        for i in range(1, len(sel.options)):
            sel.select_by_index(i)
            time.sleep(3)
            self.extract_data(self.c.page_source)
            sel = self.get_selectors()
            sel = sel[0]
        return

    def two_loops(self, dropdowns):
        sel1 = Select(dropdowns[0])
        for i in range(1, len(sel1.options)):
            sel1.select_by_index(i)
            time.sleep(3)
            sel1, sel2 = self.get_selectors()
            for k in range(1, len(sel2.options)):
                time.sleep(3)
                try:
                    sel2.select_by_index(k)
                except StaleElementReferenceException:
                    sel1, sel2 = self.get_selectors()
                    time.sleep(10)
                    sel2.select_by_index(k)

                time.sleep(3)
                self.extract_data(self.c.page_source)
                time.sleep(2)
                sel1, sel2 = self.get_selectors()
                time.sleep(2)
                if sel2.options[k] == sel2.options[-1]:
                    sel2.select_by_index(0)
                    time.sleep(3)
                    sel1, sel2 = self.get_selectors()

    def get_selectors(self):
        """
        This is a helper function.
        Every time the selector selects an option from the
        dropdown the elements previously stored in dropdows become stale,
        so there is a constant need to refresh elements
        """
        element = WebDriverWait(self.c, 30).until(
            EC.presence_of_element_located((By.CLASS_NAME, "variationDropdownPanel"))
        )

        try:
            options = self.c.find_elements_by_xpath(
            "//div/table/tbody/tr/td/div[@class='variationDropdownPanel']/select")
        except StaleElementReferenceException:
            self.c.implicitly_wait(3)
            options = self.c.find_elements_by_xpath(
                "//div/table/tbody/tr/td/div[@class='variationDropdownPanel']/select")

        return [Select(option) for option in options]

    def extract_data(self, source):
        item = {}
        html = fromstring(source)

        title = html.xpath("//div[@id='product-detail-div']/h1/text()")
        if title:
            title = title[0]
        link = self.c.current_url
        photo = html.xpath("//a[@class='MagicZoomPlus']/img/@src")
        if photo:
            photo = photo[0]
        product_detail = html.xpath(
            "//div[@class='prod-detail-part']/span[@class='prod-detail-part-value']/text()")
        if product_detail:
            product_detail = product_detail[0]

        price = html.xpath("//span[@class='prod-detail-cost-value']/text()")
        if price:
            price = price[0]

        product_option = html.xpath("//option[@selected='selected']/text()")

        options = 'No options'
        option_label = html.xpath("//span[@class='label']/text()")
        if len(product_option) == 1 and len(option_label) == 1:
            options = option_label[0].strip() + product_option[0].strip()
        elif len(product_option) > 1:
            options = '||'.join(
                [option_label[i].strip() + product_option[i].strip() for i in range(
                    min(len(option_label), len(product_option)))
                 ])

        if not product_option and not option_label:
            item['Title'] = title
            item['Link'] = link
            item['Photo'] = photo
            item['Price'] = price
            item['Item_number'] = product_detail if product_detail else 'No product detail'
            item['Options'] = options if options else 'No options'
        else:
            item['Title'] = title
            item['Link'] = link
            item['Photo'] = photo
            item['Item_number'] = product_detail if product_detail else 'No product detail'
            item['Price'] = price
            item['Options'] = options if options else 'No options'
        return self.write_to_csv(item)

    def write_to_csv(self, item):
        with open('NewMemorialsDirect.csv', 'ab') as f:
            w = csv.DictWriter(f, item.keys(), delimiter=' ')
            if not self.file_exists:
                w.writeheader()
                self.file_exists = True
            w.writerow(item)

NewMemorialsDirectItemSpider()